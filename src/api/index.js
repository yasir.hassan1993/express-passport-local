const express = require('express');

const userRoutes = require('./resources/users/users.routes');

const router = express.Router();

router.get('/', (req, res) => {
    res.json({
        message: 'API - 👋🌎🌍🌏',
    });
});

router.use('/users', userRoutes);

module.exports = router;