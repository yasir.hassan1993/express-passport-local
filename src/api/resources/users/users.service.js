const User = require('./users.models');

const findAll = async () => {
    try {
        const users = await User.find({});
        console.log(users);
        return users;
    } catch (err) {
        throw new Error('Error while paginating users, ', err);
    }
};

const findByEmail = async (email) => {
    try {
        const user = await User.findOne({ email }, '_id email').exec();
        console.log(user);
        return user
    } catch (err) {
        throw new Error('Error while paginating user, ', err);
    }
}

module.exports = {
    findAll,
    findByEmail,
};