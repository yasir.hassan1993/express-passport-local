const express = require('express');

const { addUser, getUsers, findUserByEmail } = require('./users.controllers');

const router = express.Router();

router.get('/', getUsers);
router.post('/', addUser);
router.post('/findByEmail', findUserByEmail);

module.exports = router;