const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UserSchema = Schema({
    email: { type: String, unique: true, required: true, lowercase: true },
    password: { type: String },
});

const User = mongoose.model('users', UserSchema);

module.exports = User;
