const User = require('./users.models');
const { findAll, findByEmail } = require('./users.service');

const addUser = async (req, res, next) => {
    try {
        const newUser = new User({
            'email': req.body.email,
            'password': req.body.password
        });

        const savedUser = await newUser.save();
        console.log(savedUser);
        res.status(201);
        res.json({
            message: 'User saved',
        });
    } catch (err) {
        res.status(500);
        next(err);
    }
};

const getUsers = async (req, res, next) => {
    try {
        const users = await findAll();
        res.status(200);
        res.json({
            users,
        });
    } catch (err) {
        res.status(500);
        next(err);
    }
}

const findUserByEmail = async (req, res, next) => {
    try {
        console.log(req.body);
        const user = await findByEmail(req.body.email);
        res.status(200);
        res.json({
            user,
        });
    } catch (err) {
        res.status(500);
        next(err);
    }
}

module.exports = {
    addUser,
    getUsers,
    findUserByEmail,
};