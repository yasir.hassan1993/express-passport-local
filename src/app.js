const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const helmet = require('helmet');
require('dotenv').config();

const api = require('./api');
const { notFound, errorHandler } = require('./api/middlewares/error-handlers');

mongoose.connect(`mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`);

const app = express();

app.use(morgan('common'));
app.use(helmet());
app.use(express.json());

app.get('/', (req, res) => {
    res.json({
        message: '🦄🌈✨👋🌎🌍🌏✨🌈🦄',
    })
});

app.use('/api/v1', api);

app.use(notFound);
app.use(errorHandler);

module.exports = app;